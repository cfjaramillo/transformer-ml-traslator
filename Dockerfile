FROM python:3.8-slim-buster
RUN pip install flask
RUN pip install numpy
RUN pip install pandas
RUN pip install tensorflow
RUN pip install keras_transformer

COPY . .
ENTRYPOINT FLASK_APP=/app.py flask run --host=0.0.0.0