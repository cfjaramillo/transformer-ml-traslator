
from flask import Flask, render_template, url_for, request
import numpy as np
from keras_transformer import get_model, decode
from pickle import load
np.random.seed(0)


# CARGAR DATASET
dataset = load(open('english-spanish.pkl', 'rb'))


# TOKENIZAR FRASES AMBOS IDIOMAS
source_tokens = []
for sentence in dataset[:, 0]:
    source_tokens.append(sentence.split(' '))

target_tokens = []
for sentence in dataset[:, 1]:
    target_tokens.append(sentence.split(' '))


# FUNCION DICCIONARIO FRASES
def build_token_dict(token_list):
    token_dict = {
        '<PAD>': 0,
        '<START>': 1,
        '<END>': 2
    }
    for tokens in token_list:
        for token in tokens:
            if token not in token_dict:
                token_dict[token] = len(token_dict)
    return token_dict


# DICCIONARIOS TOKENS Y DICCIONARIO INVERSO
source_token_dict = build_token_dict(source_tokens)
target_token_dict = build_token_dict(target_tokens)
target_token_dict_inv = {v: k for k, v in target_token_dict.items()}


# AGREGAR START, END y PAD a cada frase del set de entrenamiento
encoder_tokens = [['<START>'] + tokens + ['<END>'] for tokens in source_tokens]
decoder_tokens = [['<START>'] + tokens + ['<END>'] for tokens in target_tokens]
output_tokens = [tokens + ['<END>'] for tokens in target_tokens]

source_max_len = max(map(len, encoder_tokens))
target_max_len = max(map(len, decoder_tokens))

encoder_tokens = [tokens + ['<PAD>'] *
                  (source_max_len-len(tokens)) for tokens in encoder_tokens]
decoder_tokens = [tokens + ['<PAD>'] *
                  (target_max_len-len(tokens)) for tokens in decoder_tokens]
output_tokens = [tokens + ['<PAD>'] *
                 (target_max_len-len(tokens)) for tokens in output_tokens]

encoder_input = [list(map(lambda x: source_token_dict[x], tokens))
                 for tokens in encoder_tokens]
decoder_input = [list(map(lambda x: target_token_dict[x], tokens))
                 for tokens in decoder_tokens]
output_decoded = [list(map(lambda x: [target_token_dict[x]], tokens))
                  for tokens in output_tokens]


# RED TRANSFORMER
model = get_model(
    token_num=max(len(source_token_dict), len(target_token_dict)),
    embed_dim=32,
    encoder_num=2,
    decoder_num=2,
    head_num=4,
    hidden_dim=128,
    dropout_rate=0.05,
    use_same_embed=False,
)
model.compile('adam', 'sparse_categorical_crossentropy')


# ENTRENAMIENTO
x = [np.array(encoder_input), np.array(decoder_input)]
y = np.array(output_decoded)
# model.fit(x,y, epochs=15, batch_size=32)
model.load_weights('./Weights.h5')


# FUNCION TRADUCIR
def translate(sentence):
    sentence_tokens = [tokens + ['<END>', '<PAD>']
                       for tokens in [sentence.split(' ')]]
    tr_input = [list(map(lambda x: source_token_dict[x], tokens))
                for tokens in sentence_tokens][0]
    decoded = decode(
        model,
        tr_input,
        start_token=target_token_dict['<START>'],
        end_token=target_token_dict['<END>'],
        pad_token=target_token_dict['<PAD>']
    )

    result = ' '.join(map(lambda x: target_token_dict_inv[x], decoded[1:-1]))
    return result


# WEBAPP
app = Flask(__name__)


@app.route("/")
def hello():
    return render_template('index.html')


@app.route('/result', methods=['POST', 'GET'])
def result():
    output = request.form.to_dict()
    name = output["name"]

    traduccion = translate(name)

    return render_template('index.html', name=traduccion)

app.run()
